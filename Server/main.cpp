#include <iostream>
#include <memory>
#include "IServerApp.h"

using namespace std;

int main(int argc, char *argv[])
{
	try
	{
		//ServerApp server;
		std::unique_ptr<IServerApp> server(CreateServerApp());
		int port =6000;
		if(argc > 1)
		{
			port = atoi(argv[1]);
		}
		server->setPort(port);
		server->Run();
	} 

	catch(std::exception& exc)
	{
		cerr << exc.what();
	}
	catch(...)
	{
		cerr << "unknown exception occured!!";
	}
	return 0;
}
