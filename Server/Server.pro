TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../SocketLib/release/ -lSocketLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../SocketLib/debug/ -lSocketLib
else:unix: LIBS += -L$$OUT_PWD/../SocketLib/ -lSocketLib

INCLUDEPATH += $$PWD/../SocketLib
DEPENDPATH += $$PWD/../SocketLib
