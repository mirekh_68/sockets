#ifndef CLIENTWINDOW_H
#define CLIENTWINDOW_H

#include <QMainWindow>
#include "SocketApplication.h"

namespace Ui {
	class ClientWindow;
}

class IClientApp;

class ClientWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit ClientWindow(QWidget *parent = 0);
	~ClientWindow();

protected:
//  bool shouldStop() override;
//  void reportConnectionAttempt() override;
//  std::string getString() override;
//  void reportStartWaiting() override ;

private slots:
	void on_pushButton_clicked();

private:
	Ui::ClientWindow *ui;
	IClientApp* mClientApp;

};

#endif // CLIENTWINDOW_H
