#include "ClientWindow.h"
#include "ui_ClientWindow.h"
#include <QMessageBox>
#include <string>
#include <iostream>
#include "defs.h"
#include "IClientApp.h"

#ifdef WIN32
#include <conio.h>
#else
int  kbhit(void);
char getch();
#endif

using std::string;
using std::cout;
using std::cin;

ClientWindow::ClientWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::ClientWindow),
	mClientApp(CreateClientApp())
{
	ui->setupUi(this);
}

ClientWindow::~ClientWindow()
{
	delete ui;
	delete mClientApp;
}

void ClientWindow::on_pushButton_clicked()
{
	QString qaddress= ui->lineEdit->text();
	std::string addr =qaddress.toStdString();
	int port = ui->portSpinBox->value();
	mClientApp->SetServer(addr,port);
	mClientApp->Run();
}

#if 0
bool ClientWindow::shouldStop()
{
	if(m_State==STATE::WAITING)
	{
		if(kbhit()){
		getch();
	  return true;
	}
  }
else if(m_State==STATE::WORKING)
  {
	if(m_StringToSend =="quit")
	  return true;
  }
  return false;
}

void ClientWindow::reportConnectionAttempt()
{
  //cout << '.';
}

string ClientWindow::getString()
{
  cout << "\nInput any text>";
  cin >> m_StringToSend;
  return m_StringToSend;
}

void ClientWindow::reportStartWaiting()
{
  cout << "Waiting for server to accept connection... \npress any key to exit... ";
}
#endif


