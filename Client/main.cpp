#include <iostream>
#include "../SocketLib/SocketApplication.h"
using namespace std;
#include "ClientWindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
	try{
		QApplication a(argc, argv);
		ClientWindow clientwin;
		clientwin.show();
		a.exec();
	}
	catch(std::exception& exc)	
	{
		cerr << exc.what();		
	}
	catch(...)
	{
		cerr << "unknown exception occured!!";
	}
	return 0;
}
