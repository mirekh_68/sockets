#ifndef SOCKET_H
#define SOCKET_H

#include "defs.h"
#include <vector>
#include <string>

#include "buffconverter.h"

class Socket
{
public:
    Socket();
    ~Socket();
    int Connect(const std::string& serv,int serv_port);
    int Listen(int serv_port);

    template<typename T>
    int Send(const T& data);

    template<typename T>
    int Recv(T& data);

private:
    int Send(const std::vector<char>& data);
    int Recv(std::vector<char>& data);

    SOCKET m_Socket = INVALID_SOCKET;

};

template<typename T>
int Socket::Send(const T& data)
{
    std::vector<char> buff;
    Type2Buff(buff,data);
    return Send(buff);
}

template<typename T>
int Socket::Recv(T& data)
{
    std::vector<char> buff;
    int ret =0;
    if((ret=Recv(buff))!=SOCKET_ERROR)
        Buff2Type(data,buff);
    return ret;
}



#endif // SOCKET_H
