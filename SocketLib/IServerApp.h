#ifndef ISERVERAPP_H
#define ISERVERAPP_H

class IServerApp
{
public:
    virtual void Run() = 0;
    virtual void setPort(int port) = 0;
    virtual ~IServerApp() {}
};


extern "C" IServerApp* CreateServerApp();

#endif // ISERVERAPP_H
