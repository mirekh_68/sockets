#ifndef SERVERAPP_H
#define SERVERAPP_H

#include "SocketApplication.h"
#include "IServerApp.h"

class ServerApp : public IServerApp, public SocketApplication
{
public:
  ServerApp();
  void Run() override;
  void setPort(int port) override {m_Port = port;}

protected:
  Socket m_Socket;

private:
  int m_Port;
};

#endif // SERVERAPP_H
