#include "SocketApplication.h"
#include <stdexcept>
#include "defs.h"
#include <iostream>


#ifdef WIN32
#include <conio.h>
#else
int  kbhit(void);
char getch();
#endif

using namespace std;
SocketApplication::SocketApplication()
{
#ifdef WIN32
	WSADATA wsadata;
	int res = WSAStartup(MAKEWORD(2, 2), &wsadata);
  if (res != NO_ERROR)
	throw std::runtime_error("Error initalizing sockets!!");
#endif
}

SocketApplication::~SocketApplication()
{
 #ifdef WIN32
	WSACleanup();
#endif
}


