#include "ClientApp.h"

void sleep_ms(int milliseconds) ;

ClientApp::ClientApp()
{
}

void ClientApp::Run()
{
    if(m_Socket.Connect(m_ServAddress, m_Port)==SOCKET_ERROR)
    {
        //cout << "Waiting for server to accept connection... \npress any key to exit... ";
        while(m_Socket.Connect(m_ServAddress, m_Port)==SOCKET_ERROR)
        {
          sleep_ms(500);
          if(shouldStop())
              return;
          else
            reportConnectionAttempt();
         }
    }
    //m_State = STATE::WORKING;
        //throw std::runtime_error("Client could not connect to the server!!");
//    std::string input;
//    while("quit"!=input) {
//        cout << "\nInput any text>";
//        cin >> input;
//        m_Socket.Send(input);
//    }
  while(true)
  {
    std::string str = getString();
    if(shouldStop())
      return;
    m_Socket.Send(str);
  }
}

std::string ClientApp::getString() const
{
    return " Sample string... \n";
}

IClientApp* CreateClientApp()
{
    return new ClientApp();
}

