#ifndef ICLIENTAPP_H
#define ICLIENTAPP_H
#include <string>

class IClientApp
{
public:
    virtual void Run() =0;
    virtual void SetServer(const std::string& serv_addr, int port)=0;
    virtual ~IClientApp() {}
};

extern "C" IClientApp* CreateClientApp();

#endif // ICLIENTAPP_H
