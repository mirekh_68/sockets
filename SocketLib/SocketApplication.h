#ifndef SOCKETAPPLICATION_H
#define SOCKETAPPLICATION_H

#include "Socket.h"

class SocketApplication
{
public:
  SocketApplication();
  ~SocketApplication();
  virtual void Run() = 0;

protected:
  Socket m_Socket;
};

#endif // SOCKETAPPLICATION_H
