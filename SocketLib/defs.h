#ifndef DEFS_H
#define DEFS_H

#ifdef WIN32
#   include <winsock2.h>
#else
#   define SOCKET int
#   define INVALID_SOCKET -1
#   define SOCKET_ERROR -1
#   define closesocket close
#   define SOCKADDR_IN sockaddr_in
#   define SOCKADDR sockaddr
#   include <sys/types.h>
#   include <sys/socket.h>
#endif




#endif // DEFS_H

