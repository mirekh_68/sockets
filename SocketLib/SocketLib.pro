#-------------------------------------------------
#
# Project created by QtCreator 2016-05-13T11:57:34
#
#-------------------------------------------------

QT       -= core gui
CONFIG += console c++11

TARGET = SocketLib
TEMPLATE =  lib
unix:CONFIG += staticlib

DEFINES += SOCKETLIB_LIBRARY
SOURCES += Socket.cpp \
    SocketApplication.cpp \
    system.cpp \
    ServerApp.cpp \
    ClientApp.cpp

unix: SOURCES+=kbhit.cpp

HEADERS +=  Socket.h \
    defs.h \
    SocketApplication.h \
    buffconverter.h \
    IServerApp.h \
    ServerApp.h \
    IClientApp.h \
    ClientApp.h

message("QMAKESPEC="$${QMAKESPEC})

win32 {
  LIBS += -lWS2_32
  DEF_FILE = SocketLib.def
}
unix {
    target.path = /usr/lib
    INSTALLS += target
}
