#ifndef CLIENTAPP_H
#define CLIENTAPP_H

#include "IClientApp.h"
#include "SocketApplication.h"
#include <string>


class ClientApp : public IClientApp, public SocketApplication
{
public:
    ClientApp();
    void Run() override;
    void SetServer(const std::string& serv_addr, int port) override
    {
        m_ServAddress=serv_addr;
        m_Port = port;
    }

private:
  std::string m_ServAddress;
  int m_Port;
  bool shouldStop() const {return false;}
  void reportConnectionAttempt() {}
  std::string getString() const ;
};

#if 0
class ClientApplication : public SocketApplication
{
public:
  ClientApplication();
  void Run() override;
  void SetServer(const std::string& serv_addr, int port){
      m_ServAddress=serv_addr;
      m_Port = port;
  }

protected:
  enum class STATE {INITIAL,WAITING, WORKING};
  STATE m_State = STATE::INITIAL;
  Socket m_Socket;
  std::string m_StringToSend;
  virtual bool shouldStop() =0;
  virtual void reportConnectionAttempt() =0;
  virtual void reportStartWaiting()=0;
  virtual std::string getString() = 0;

private:
  std::string m_ServAddress;
  int m_Port;
};

class ConsoleClientApllication : public ClientApplication
{
protected:
  bool shouldStop() override;
  void reportConnectionAttempt() override;
  std::string getString() override;
  void reportStartWaiting() override ;
};

#endif

#endif // CLIENTAPP_H
