#ifndef BUFFCONVERTER_H
#define BUFFCONVERTER_H
#include <vector>
#include <string>
#include <type_traits>

#ifndef WIN32
#include <cstring>
#endif

#define ASSERT(cond)

template<typename T,bool is_trivially_copyable>
class BuffConverterBase
{
};

template<typename T>
class BuffConverterBase<T,true>
{
public:
    void convert(std::vector<char>& buff,const T& data){
        int size = sizeof(T);
        buff.resize(size_t(size));
        memcpy(&buff[0],&data,size);
    }

    void convert(T& data,const std::vector<char>& buff){
        int size = sizeof(T);
        ASSERT(buff.size()==size);
        memcpy(&buff[0],&data,size);
    }

};


template<bool any_val>
class BuffConverterBase<std::string,any_val>
{
    typedef std::string T;
public:
    void convert(std::vector<char>& buff,const T& data){
        int size = data.size();
        buff.resize(size_t(size));
        memcpy(&buff[0],&data[0],size); // todo: verify if this workd for strings!
    }

    void convert(T& data,const std::vector<char>& buff){
        int size = buff.size();
        data.resize(size);
        memcpy(&data[0],&buff[0],size);
    }

};


template<typename T>
class BuffConverter : public BuffConverterBase<T,std::is_trivial<T>::value>
{

};

template<typename T>
void Type2Buff(std::vector<char>& buff,const T& data)
{
    BuffConverter<T> bconv;
    bconv.convert(buff,data);

}

template<typename T>
void Buff2Type(T& data,const std::vector<char>& buff)
{
    BuffConverter<T> bconv;
    bconv.convert(data,buff);
}

#endif // BUFFCONVERTER_H
