#include "Socket.h"
#include <cstring>
#ifndef WIN32
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif


Socket::Socket()
{
    m_Socket = ::socket(AF_INET, SOCK_STREAM, 0);
}

Socket::~Socket()
{
    if(m_Socket!= INVALID_SOCKET)
        closesocket(m_Socket);
}

int Socket::Connect(const std::string &serv_addr, int serv_port)
{
    SOCKADDR_IN addrSrv;
    addrSrv.sin_family = AF_INET;
    addrSrv.sin_port = htons(serv_port);
    addrSrv.sin_addr.s_addr = inet_addr(serv_addr.c_str());
    return ::connect(m_Socket,(SOCKADDR*)&addrSrv, sizeof(SOCKADDR_IN));
}

int Socket::Listen(int serv_port)
{
    sockaddr_in service;
    memset(&service,0,sizeof(sockaddr_in));
    service.sin_family =AF_INET;
    service.sin_port = htons(serv_port);

    if(::bind(m_Socket,(SOCKADDR*)&service,sizeof(service))==SOCKET_ERROR)
    {
        printf("bind failed\n");
        ::closesocket(m_Socket);
        return 1;
    }

    if(::listen(m_Socket,1)==SOCKET_ERROR)
        return 1;

    SOCKET acceptSocket = SOCKET_ERROR;
    printf( "Waiting for a client to connect...\n" );

    while( acceptSocket == SOCKET_ERROR )
    {
        acceptSocket = accept( m_Socket, NULL, NULL );
    }

    printf( "Client connected.\n" );
    m_Socket = acceptSocket;
    return 0;
}

int Socket::Send(const std::vector<char> &data)
{
    size_t size = data.size();
    ::send(m_Socket, (char*)&size,sizeof(size_t),0);
    ::send(m_Socket, &data[0],size,0);
    return 0;
}

int Socket::Recv(std::vector<char> &data)
{
    size_t size =0;
    ::recv(m_Socket, (char*)&size,sizeof(size_t),0);
    data.resize(size);
    return ::recv(m_Socket, &data[0],size,0);
}

